package com.dojchinovski.ctu.ddw;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

/**
 * 
 * WordCount example reads text files and counts how often words occur.
 * The input is text files and the output is text files, each line of 
 * which contains a word and the count of how often it occured, separated by
 * a tab.
 * 
 * @author milan@dojchinovski.mk
 * 
 */
public class WordCount {
    
    public static class Map extends Mapper<LongWritable, Text, Text, IntWritable> {
      
        private final static IntWritable one = new IntWritable(1);       
        private Text word = new Text();
           

        public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        
            String line = value.toString();
            
           // Iterate over all of the words in the string
            StringTokenizer tokenizer = new StringTokenizer(line);            
            while (tokenizer.hasMoreTokens()) {                
                word.set(tokenizer.nextToken());
                context.write(word, one);
            }
        }
    } 
           
    public static class Reduce extends Reducer<Text, IntWritable, Text, IntWritable> {
   
        public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            
            // Iterate over all of the values (counts of occurrences of this word)
            int sum = 0;
           
            for (IntWritable val : values) {
                // Add the value to our counter
                sum += val.get();
            }
            context.write(key, new IntWritable(sum));
        }
    }
           
    public static void main(String[] args) throws Exception {
                
        // Create a configuration
        Configuration conf = new Configuration();
        
        // Create a job with name "WordCount" from the default configuration that will use the WordCount class
        Job job = new Job(conf);        
        job.setJobName("WordCount");
       
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        
        // Configure the job: name, mapper, reducer
        job.setMapperClass(Map.class);
        job.setReducerClass(Reduce.class);
           
        // Configure the input and output
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
           
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        
        // Submit the job, then poll for progress until the job is complete
        job.waitForCompletion(true);
    }
}
